$done({
  body: JSON.stringify({
    expire: "9692-01-01T00:00:00Z",
    start: "2024-02-27T00:00:00Z",
    isExpired: false,
    isGracePeriodExpired: false,
    inAppPurchaseAllowed: true,
    tier: {
      id: "gold",
      rank: 5,
      product: [
        {
          id: "renewable.premiumgold.annual",
          sku: "renewable.premiumgold.annual",
          contentType: "subscription",
          productType: "GoldYearly",
          paymentProvider: "Apple",
          rank: 6,
          clientProductMetadata: {
            selectionRank: 5,
            displayOrder: 5,
            isEntitledPremiumScreenProduct: false
          }
        }
      ],
      feature: [
        // Free features
        {
          id: "spam_blocking",
          rank: -2147483648,
          status: "Included",
          isFree: true
        },
        {
          id: "caller_id",
          rank: -2147483648,
          status: "Included",
          isFree: true
        },
        // Premium features
        {
          id: "premium_feature",
          rank: -2147483648,
          status: "Included",
          isFree: false
        },
        {
          id: "verified_badge",
          rank: -2147483648,
          status: "Included",
          isFree: false
        },
        {
          id: "siri_search",
          rank: 1,
          status: "Included",
          isFree: false
        },
        {
          id: "no_ads",
          rank: 2,
          status: "Included",
          isFree: false
        },
        {
          id: "extended_spam_blocking",
          rank: 3,
          status: "Included",
          isFree: false
        },
        {
          id: "identifai",
          rank: 5,
          status: "Included",
          isFree: false
        },
        {
          id: "who_viewed_my_profile",
          rank: 11,
          status: "Included",
          isFree: false
        },
        {
          id: "incognito_mode",
          rank: 13,
          status: "Included",
          isFree: false
        },
        {
          id: "premium_badge",
          rank: 17,
          status: "Included",
          isFree: false
        },
        {
          id: "premium_support",
          rank: 18,
          status: "Included",
          isFree: false
        },
        {
          id: "live_chat_support",
          rank: 19,
          status: "Included",
          isFree: false
        },
        {
          id: "premium_app_icon",
          rank: 21,
          status: "Included",
          isFree: false
        },
        {
          id: "gold_caller_id",
          rank: 22,
          status: "Included",
          isFree: false
        }
      ]
    }
  })
});